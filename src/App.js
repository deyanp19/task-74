import "./App.css";
import {
  useState,
  useMemo
} from 'react';


function App() {
  const filterNum = (num) => num % 2==0
  
  const [text, setNumb] = useState(0);
  const boolNumVal = useMemo(() =>{ let result =filterNum(text.numb)==false;
  console.log(result);
    return result;
  },  [text]);

  console.log(text,boolNumVal,filterNum(text.numb)==true);
  const handleInput = (e) => {
    setNumb({
      numb: e.target.value
    })
    console.log(text);
  }


  return ( <
    div className = "App" >
    <
    div className = "control has-icons-right" >
    <input onChange={handleInput} className= "input is-large" type="text" placeholder="Enter number..."/>
    <
    span className = "icon is-small is-right" >

    {
      boolNumVal ? ( < i className = "fas  fa-times" / > ): ( < i className = "fas  fa-check" / > ) 
    } <
    /span>  <
    /div> <
    /div>
  );
}

export default App;